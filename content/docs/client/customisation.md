---
title: "Customisation"
description: "How to customise the Cactus Comments client?"
lead: ""
date: 2020-10-06T08:49:31+00:00
lastmod: 2022-01-02T17:13:00+00:00
draft: false
images: []
menu:
  docs:
    parent: "client"
weight: 22
toc: true
---

## Styling

Cactus Comments comes with a style sheet that aims to look good on most webpages. If you want to customise the look and feel of Cactus Comments to your own liking, don't hesitate to build your own style sheet. A good starting place is to get the [provided stylesheet](https://gitlab.com/cactus-comments/cactus-client/-/blob/main/src/style.css) and modify it to your needs.

If you do change the stylesheet, please consider using a [version-specific link to the web client javascript](https://gitlab.com/cactus-comments/cactus-client/-/releases), to avoid breaking changes that might occur on [latest.cactus.chat](https://latest.cactus.chat).

### Customisation

Cactus Comments supports CSS variables. This allows you to customise Cactus Comments to your own liking, without knowing about its internal structure. You can set them on any element higher up in the document tree than Cactus Comments, but a typical place to put them is the `<html>` element. For example, you could customise the button colors of Cactus Comments like this

```css
:root {
  --cactus-button-color: limegreen;
}
```

As of client version v0.11.0, the following CSS variables are supported

- `--cactus-border-width`
- `--cactus-border-radius`
- `--cactus-text-color`
- `--cactus-text-color--soft`
- `--cactus-background-color`
- `--cactus-background-color--strong`
- `--cactus-border-color`
- `--cactus-box-shadow-color`
- `--cactus-button-color`
- `--cactus-button-color--strong`
- `--cactus-button-color--stronger`
- `--cactus-error-color`

Cactus Comments uses defaults that should look good on most sites, so you don't need to set them.

Note, if you set them make sure your style sheet is placed *after* the Cactus Comments style sheet in the markup, otherwise your declarations will be overwritten in the CSS cascade.

### Dark Mode

Cactus Comments comes with presets for light and dark mode. By default, the stylesheet adapts to the browser's chosen color scheme, so you don't need to do anything. If you want to control light / dark mode explicitly (e.g. if your website has a toggle button), you can overwrite this by setting a `light` and `dark` class on the `<html>` element.

You can even use both together. For example, you could set a light/dark class on the `<html>` element when the user presses your toggle button for the first time.

Your colorscheme toggle code might look like this:

```js
// switches between .light and .dark classes
// if no class is present (initial state), then assume current state based on system color scheme
// if system color scheme is not supported, then assume current state is light
function toggle() {
    if (document.documentElement.classList.contains("light")) {
      document.documentElement.classList.remove("light")
      document.documentElement.classList.add("dark")
    } else if (document.documentElement.classList.contains("dark")) {
      document.documentElement.classList.remove("dark")
      document.documentElement.classList.add("light")
    } else {
      if (window?.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.documentElement.classList.add("light")
      } else {
        document.documentElement.classList.add("dark")
      }
    }
}
```

And your CSS for the page might look like this

```css
/* automatic / manual light mode */
:root, :root.light {
    background-color: white;
    color: black;
}

/* automatic dark mode */
/* ❗️ keep these rules in sync with the manual dark mode below! */
@media (prefers-color-scheme: dark) {
    :root {
    background-color: black;
    color: white;
    }
}

/* manual dark mode 
/* ❗️ keep these rules in sync with the automatic dark mode above! */
:root.dark {
    background-color: black;
    color: white;
}
```

### Comment Order

Using Flexbox it's easy to customise the order of Cactus Comment elements without needing to change the markup.

To reverse the order of the comments list and the editor, putting the comments list at the top and the editor at the bottom, you can use the rule

```css
.cactus-container {
  flex-direction: column-reverse;
}
```

To reverse the order of the comments themselves, putting older ones at the top and newer ones at the bottom, you can use the rules

```css
.cactus-comments-container {
  flex-direction: column-reverse;
}

.cactus-comments-list {
  flex-direction: column-reverse;
}
```

(Note, Safari 14 currently has a [known bug](https://bugs.webkit.org/show_bug.cgi?id=225278) which leads to missing gaps between the sections if these rules are used.)


## Releases

If you want to include the newest release of Cactus Comments on your website,
you should use the `latest` links:

```
<script type="text/javascript" src="https://latest.cactus.chat/cactus.js"></script>
<link rel="stylesheet" href="https://latest.cactus.chat/style.css" type="text/css">
```

If you want to pin it to a specific version, check out [the Gitlab releases
page](https://gitlab.com/cactus-comments/cactus-client/-/releases).

(Note, Safari 14 currently has a [known bug](https://bugs.webkit.org/show_bug.cgi?id=225278) which leads to missing gaps between the sections if these rules are used.)
